# SAE103 - Installer un système

## Mon GitLab

Voici mon [GitLab](https://gitlab.com/KorentinGeorget) où vous trouverez le projet SAE103 - Installer un système, vous y trouverez tout les fichiers relatifs a cette SAE.

## Objectif :
Installer un environnement linux en double-boot.

## Matériel requis :

- Une clé USB vierge
- le logiciel [Rufus](https://rufus.ie/fr/)
- la version voulue de linux en iso ( ici [Iso Xubuntu](http://ftp.free.fr/mirrors/ftp.xubuntu.com/releases/22.04/release/xubuntu-22.04.1-desktop-amd64.iso))

## Transformer la clé USB vierge en clé USB bootable avec l'environnement linux

Lancer Rufus et mettre les paramètres suivants :

- À `dispositif` mettre sélectionner votre clé USB vierge
- À `type` de démarrage sélectionner le fichier iso de l'environnement
- À `schéma` de partition choisir GPT
- À `système` de destination choisir UEFI

Ne modifier pas la section `Option de Formatage`.

## Partitionner le disque pour faire de la place pour notre nouvelle environnement

Faire un dual-boot signifie qu'il y'a en même temps un enviornnement Windows et un environnement Linux, pour faire cela il faut donc assez de place pour installer l'environnement Linux, on va donc partitionner le disque dur de l'ordinateur pour lui libérer de l'espace qu'on utilisera pour Linux.
Sur votre Windows, aller dans vos fichiers et aller dans la gestion de l'ordinateur clique droit sur `Ce PC` et cliquer sur `gerer`, ensuite aller dans la section `gestion des disques` ensuite séléctionner le disque que vous voulez partitionner, clique droit dessus et séléctionner `Réduire le volume`. Choissisez la taille que vous voulez partitionner ( il faut au moins 50GB pour que l'environnement Linux fonctionne sans problème) et cliquez sur `reduire`.

## Installer et configurer son nouvelle environnement
### Configurer le BIOS
Brancher votre clé nouvelle USB bootable sur votre ordinateur et redémarrer le.

Au démarrage de votre ordinateur, allumez votre bios, dirigez-vous vers boot option et désactivez l'option `Secure boot`.

Ensuite allez dans le `Boot menu` et séléctionner votre clé USB. Votre ordinateur va se lancer avec votre nouvelle environnement.


### Installation de l'environnement

Votre ordinateur devrait démarrer sur l'environnement présent sur la clé USB et affichez un premiere page d'installation. Ici cliquez sur le choix `Try or install Xubuntu`. Après un court chargement une nouvelle page apparait et demande de saisir votre langue, une fois cela fait cliquez sur `Installer Xubuntu`.
Ensuite laissez par défaut la disposition du clavier puis cliquez sur continuez, ne vous connectez pas a internet  vous le ferez plus tard, il y'a des risque que l'installation échoue si vous le faites.
Puis une fois sur la page `Mise à jour et autres logiciels` séléctionner `Installation normale` et séléctionner également `Installer logiciels tier ...`.
Enfin nous arrivont sur la page `Type d'installation`, ici séléctionner `Autre chose` puis chercher la partie libre du disque que vous avez partitionner plus tôt, séléctionner la avec le `+` et choissisez combien de GB vous voulez lui attribuez (ça sera le lieu d'installation de l'environnement donc je conseille la totalité de l'espace disponnible - 10GB), **attention** ne lui attribuez pas tout l'espace disponnible, gardez entre 5-10GB, puis séléctionner de l'utilisez comme `Ext4 ...`. Séléctionner maintenant le reste d'espace que vous avez laisser et reséléctionner le avec le `+` cette fois attribuez tout l'espaces restant et séléctionne de l'utilisez comme `Zone d'échange`.
Enfin cliquez sur `Installer maintenant` et remplissez la page `Qui êtes vous?` avec vos informations personnelles. Pour finir cliquez sur `Continuer` et l'installation de votre environnement se fera après quelque minutes d'attentes.

## Configuration personnelles de l'environnement

J'ai personnellement décidé d'installer des applications supplèmentaires après mon installation.

**Liste d'application et commande pour les installer:**

Docker (`sudo snap install docker`) -> donner les droit a docker (`sudo chmod 777 /var/run/docker.sock`)

Java (`sudo apt install default-jdk`)

Python(`sudo apt install python3`)

VsCode (`sudo snap install -classic code`)

Extension VsCode (`code --install-extension ms-python.python`) et (`code --install-extension njpwerner.autodocstring`)


